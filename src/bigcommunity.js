const BigCommunitySection = document.createElement('section');
BigCommunitySection.setAttribute('id', 'community-section');
BigCommunitySection.classList.add('bigcommunitysection');

const BigCommunitySectionH2 = document.createElement('h2');
BigCommunitySectionH2.textContent = 'Big Community of \n People Like You';
BigCommunitySectionH2.classList.add('BigCommunitySection-h2');

const BigCommunitySectionP = document.createElement('p');
BigCommunitySectionP.textContent = 'We’re proud of our products, and we’re really excited \n when we get feedback from our users.';
BigCommunitySectionP.classList.add('BigCommunitySection-p');

const BigCommunitySectionDiv = document.createElement('div');
BigCommunitySectionDiv.setAttribute('id', 'BigCommunitySectionDiv');
BigCommunitySectionDiv.classList.add('BigCommunitySection-div');

const BigCommunitySectionList = document.createElement('ul');
BigCommunitySectionList.setAttribute('id', 'community-list');
BigCommunitySectionList.classList.add('BigCommunitySection-ul');

BigCommunitySectionDiv.appendChild(BigCommunitySectionList);
BigCommunitySection.appendChild(BigCommunitySectionH2);
BigCommunitySection.appendChild(BigCommunitySectionP);
BigCommunitySection.appendChild(BigCommunitySectionDiv);

const LearnMore = document.getElementById('LearnMore');
LearnMore.parentNode.insertBefore(BigCommunitySection, LearnMore);

export { BigCommunitySection };
