let dataBatch = [];
const batchSize = 5;

self.addEventListener('message', event => {
  if (event.data.type === 'click') {
    dataBatch.push(event.data.target);

    if (dataBatch.length >= batchSize) {
      sendDataToServer(dataBatch);
      dataBatch = [];
    }
  }
});

function sendDataToServer(batch) {
  fetch('/analytics/user', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(batch),
  });
}
