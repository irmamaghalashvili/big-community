import { expect } from 'chai';
import sinon from 'sinon';
import { validate, validateWithThrow, validateWithLog, validateAsync } from '../email-validator.js';


describe('validate', () => {
  it('should return true for valid email endings', () => {
    expect(validate('user@gmail.com')).to.equal(true);
    expect(validate('user@outlook.com')).to.equal(true);
    expect(validate('user@yandex.ru')).to.equal(true);
  });

  it('should return false for invalid email endings', () => {
    expect(validate('user@example.com')).to.equal(false);
    expect(validate('user@unknown.com')).to.equal(false);
  });
});

describe('validateWithThrow', () => {
  it('should return true for valid email endings', () => {
    expect(validateWithThrow('user@gmail.com')).to.equal(true);
  });

  it('should throw an error for invalid email endings', () => {
    expect(() => validateWithThrow('user@example.com')).to.throw('Invalid email format');
  });
});

describe('validateWithLog', () => {
    let consoleLogStub; 

    beforeEach(() => {
      consoleLogStub = sinon.stub(console, 'log'); 
    });
  
    afterEach(() => {
      consoleLogStub.restore();
    });
  
    it('should log and return true for valid email endings', () => {
      const result = validateWithLog('user@gmail.com');
      expect(result).to.equal(true);
      expect(consoleLogStub.calledOnce).to.be.true;
    });
  
    it('should log and return false for invalid email endings', () => {
      const result = validateWithLog('user@example.com');
      expect(result).to.equal(false);
      expect(consoleLogStub.calledOnce).to.be.true;
    });
  });

describe('validateAsync', () => {
  it('should return true for valid email endings asynchronously', async () => {
    const result = await validateAsync('user@gmail.com');
    expect(result).to.equal(true);
  });

  it('should return false for invalid email endings asynchronously', async () => {
    const result = await validateAsync('user@example.com');
    expect(result).to.equal(false);
  });
});
