// email-validator.js
function validate(email) {
  const emailEnding = email.split('@')[1];
  const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}

function validateWithThrow(email) {
  if (!validate(email)) {
    throw new Error('Invalid email format: Email does not have a valid ending');
  }
  return true;
}

function validateWithLog(email) {
  const isValid = validate(email);
  console.log(`Email validation result for ${email}: ${isValid}`);
  return isValid;
}

async function validateAsync(email) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(validate(email));
    }, 100); // Simulate an asynchronous operation
  });
}

export { validate, validateWithThrow, validateWithLog, validateAsync };

