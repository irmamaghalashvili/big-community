module.exports = {
    extends: 'airbnb-base',
    rules: {
      'indent': ['error', 2],
      'no-console': 'off',
      'linebreak-style': 'off',
      'comma-dangle': ['error', 'never'],
      'prefer-template': 'error',
    },
  };
  