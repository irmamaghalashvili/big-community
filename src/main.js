import { joinUsSection } from "./join-us-section.js";
import { BigCommunitySection } from './bigcommunity.js';
import { validate } from "./email-validator.js";
import "./styles/style.css";

const communitySection = document.getElementById("BigCommunitySectionDiv");
if (communitySection) {
  const startTime = performance.now();

  fetch('/community')
    .then((response) => {
      if (!response.ok) {
        throw new Error('Network response was not ok.');
      }
      return response.json();
    })
    .then((data) => {
      const endTime = performance.now();
      const fetchTime = endTime - startTime;

      // Measure page load time
      const pageLoadTime = window.performance.timing.loadEventEnd - window.performance.timing.navigationStart;

      const communityList = document.getElementById("community-list");
      data.forEach((person) => {
        const listItem = document.createElement("li");
        listItem.classList.add('community-li');

        listItem.style.display = 'flex';
        listItem.style.flexDirection = 'column';

        const avatarImg = document.createElement("img");
        avatarImg.classList.add('community-avatar');
        avatarImg.src = person.avatar;
        avatarImg.alt = `${person.firstName} ${person.lastName} Avatar`;

        listItem.appendChild(avatarImg);

        const nameElement = document.createElement("span");
        nameElement.textContent = `${person.firstName} ${person.lastName}`;
        nameElement.style.fontWeight = 'bold';

        const positionElement = document.createElement("span");
        positionElement.textContent = person.position;

        listItem.appendChild(nameElement);
        listItem.appendChild(positionElement);

        communityList.appendChild(listItem);

        // Log fetchTime and pageLoadTime or send them to the server
        const metrics = {
          fetchTime: fetchTime,
          pageLoadTime: pageLoadTime,
        };
        sendDataToServer(metrics);
      });
    })
    .catch((error) => {
      console.error('Error:', error);
    });

  const LearnMore = document.getElementById('LearnMore');
  LearnMore.parentNode.insertBefore(BigCommunitySection, LearnMore);
}

function sendDataToServer(metrics) {
  fetch('/analytics/performance', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(metrics),
  })
  .then(response => {
    if (response.ok) {
      console.log('Metrics sent successfully');
    } else {
      console.error('Failed to send metrics:', response.statusText);
    }
  })
  .catch(error => {
    console.error('Error sending metrics:', error);
  });
}

const worker = new Worker("web-worker.js");
const clickableElements = document.querySelectorAll("button, a");

clickableElements.forEach(element => {
  element.addEventListener('click', event => {
    worker.postMessage({
      type: 'click',
      target: element.nodeName,
    });
  });
});

document.addEventListener("DOMContentLoaded", () => {
  const appContainer = document.getElementById("app-container");
  const standardSection = joinUsSection.createStandardProgram();
  appContainer.appendChild(standardSection);

  const emailInput = document.getElementById('email-input');
  const subscribeButton = document.getElementById('subscribe-button');
  const unsubscribeButton = document.getElementById('unsubscribe-button');

  subscribeButton.addEventListener('click', () => {
    const isSubscribed = localStorage.getItem("isSubscribed") === "true";
    const emailValue = emailInput.value;

    if (!validate(emailValue)) {
      alert('Error: Please enter a valid email address.'); // Show error for invalid email
      return;
    }
    if (isSubscribed) {
      unsubscribeButton.disabled = true;
      unsubscribeButton.style.opacity = '0.5';

      const emailToUnsubscribe = localStorage.getItem("subscriptionEmail");

      if (emailToUnsubscribe) {
        fetch('http://localhost:3000/unsubscribe', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ email: emailToUnsubscribe }),
        })
        .then((response) => {
          if (response.ok) {
            emailInput.style.display = 'block';
            emailInput.value = '';
            subscribeButton.textContent = 'Subscribe';
            localStorage.removeItem("subscriptionEmail");
            localStorage.setItem("isSubscribed", "false");
          } else {
            return response.json().then(data => {
              alert(`Error: ${data.message}`);
            });
          }
        })
        .catch((error) => {
          console.error('Error:', error);
        })
        .finally(() => {
          unsubscribeButton.disabled = false;
          unsubscribeButton.style.opacity = '1';
        });
      } else {
        alert("You are not subscribed to the community.");
      }
    } else {
      if (emailValue === 'forbidden@gmail.com') {
        alert('Error: This email is not allowed.');
        return;
      }

      emailInput.style.display = 'none';
      subscribeButton.textContent = 'Unsubscribe';
      localStorage.setItem("isSubscribed", "true");
      sendDataToServer(emailValue);
    }
  });

  function sendDataToServer(email) {
    fetch('/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: email }),
    })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else if (response.status === 422) {
        return response.json().then((data) => {
          alert(`Error: ${data.message}`);
        });
      } else {
        alert('An error occurred while subscribing. Please try again later.');
        throw new Error('Network response was not ok.');
      }
    })
    .then(() => {
      emailInput.style.display = 'block';
      emailInput.value = '';
      subscribeButton.textContent = 'Subscribe';
      localStorage.removeItem("subscriptionEmail");
      localStorage.setItem("isSubscribed", "false");
    })
    .catch((error) => {
      console.error('Error:', error);
    })
    .finally(() => {
      subscribeButton.disabled = false;
      subscribeButton.style.opacity = '1';
    });
  }


 

  unsubscribeButton.addEventListener('click', () => {
    unsubscribeButton.disabled = true;
    unsubscribeButton.style.opacity = '0.5';

    fetch('/unsubscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: localStorage.getItem("subscriptionEmail") }),
    })
    .then((response) => {
      if (response.ok) {
        emailInput.style.display = 'block';
        emailInput.value = '';
        subscribeButton.textContent = 'Subscribe';
        localStorage.removeItem("subscriptionEmail");
        localStorage.setItem("isSubscribed", "false");
      } else {
        alert('An error occurred while unsubscribing. Please try again later.');
        throw new Error('Network response was not ok.');
      }
    })
    .catch((error) => {
      console.error('Error:', error);
    })
    .finally(() => {
      unsubscribeButton.disabled = false;
      unsubscribeButton.style.opacity = '1';
    });
  });
});
